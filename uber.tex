\documentclass[a4paper, 12pt, notitlepage]{article}

\usepackage{amsfonts} 
\usepackage{graphicx} 
\usepackage{listings}


\title{Uber Assignment} 
\author{Athanasios Noulas} 
\date{\today} 

\newcommand{\shellcmd}[1]{\\\indent\indent\texttt{\footnotesize\# #1}\\}

\begin{document}
\maketitle
\section{SQL}[20 minutes]

1. Between Oct 1, 2013 at 10am PDT and Oct 22, 2013 at 5pm PDT, what percentage of requests made by unbanned clients each day were cancelled in each city?

\begin{figure}[h]
\includegraphics[width=\linewidth]{sql1}
\end{figure}
\newpage
2. For city\_ids 1, 6, and 12, list the top three drivers by number of completed trips for each week between June 3, 2013 and June 24, 2013.

\begin{figure}[ph]
\includegraphics[width=\linewidth]{sql2}
\end{figure}


\section{Experiment and metrics design}
[45 minutes] 
 
A product manager on the Growth Team has proposed a new feature. Instead of getting a
free ride for every successful invite, users will get 1 Surge Protector, which exempts them
from Surge pricing on their next surged trip.
\begin{enumerate}
\item What would you choose as the key measure of the success of the feature?
\item What other metrics would be worth watching in addition to the key indicator?
\item Describe an experiment design that you could use to confirm the hypothesis that your chosen key measure is different in the treated group.
\end{enumerate}

\subsection{TL;DR}
The questions are very interesting and at the same time generic enough to cry for a lengthy response. Nevertheless, here goes a TL;DR version:
\begin{enumerate}
\item I would measure the number of successful invites per user for (a) the current promotion method of free rides and and (b) the new proposed promotion method of Surge Protector.
\item Other metrics worthwhile watching would be revenue made in both promotion methods, percentage of users that had at least one successful invite in each promotion method, Life Time Value of the current and newly acquired users for each promotion method.
\item Our null hypothesis is that the new method is as good as the old one. We will design an experiment to try to reject the null hypothesis and thus prove that the Surge Protector promotion method is better and should be applied to all our users. 

The experiment design requires two \textit{big enough }independent random groups of users. \textit{Big enough} means that we can assume that the two populations are identical in everything except the applied treatment. The first group will be notified about the Free Ride promotion, while the second for the Surge Protector.  An outer experiment can be set up comparing the two populations to the rest of our user-base to measure the effect of our notifications too. 

We monitor all our KPIs for a predefined period of time (e.g. two weeks) and measure them at the end of the period. We then use a statistical significance test to see if the null hypothesis holds, \textit{i.e.} the new promotion method is not better than the old one, or the null hypothesis is rejected, \textit{i.e.} the new promotion method is better and should be applied to all our users.

The proposed main KPI is the number of successful invites per user and has a Poisson distribution, thus we need a one-tailed Poisson test.
\end{enumerate}

\subsection{Assumptions}
Giving away free rides or Surge protection has a negative short-term impact on the revenue of the company. I assume that the reason behind these features must be growing the user-base, engaging the existing users and in the long-run increasing the lifetime customer value of every person as Uber user. 

The underlying hypothesis behind the Product Manager's idea is that Users will like the Surge Protector benefit more than the free ride, and therefore they will invite successfully more of their friends. 

A secondary hypothesis could be that (a) the Surge Protector engages more users than the free ride, in which case we will have to measure the percentage of users that actually invited at least one friend or (b) that the Surge Protector benefits Surge-period users more, and they are our desired target group, and thus we will motivate them further to invite their friends --- in this case we need to split our groups respecting our users history, or (c) that the free ride is often given to people who don't plan to ever spend money on the service and they are consequently just abusing the promotions. 

Multiple-KPI optimisation is unnecessarily complex for this idea, but instead we assume we can focus on the main hypothesis alone. We can evaluate the alternative hypotheses based on the first experiment's results and device additional experiments to explore the ones that look most promising. 

From a statistical point of view, we assume that we have an $H_0$ that Free Rides and Surge Protector are equally good to each other. We need to collect data to see if we will reject the $H_0$ or not. 

\subsection{Long Answers to the Questions}
Although the scope of the answers doesn't change, there are many important details and considerations which are included in the longer version of the answers.

\begin{enumerate}
\item The key indicator is the number of successful new invites per user. Note that this variable has a Poisson distribution \footnote{If we can assume that the number of invites is much smaller than the number of users, this can be approximated with a binomial distribution and if we just measure how many users engage to the promotions and successfully invite at least one friend we can further simplify the variable to a Bernoulli distribution.}

\item There are multiple secondary indicators that can help us validate the original hypothesis, measure the benefits for the company and evaluate the possible follow up experiments. More specifically:
\begin{itemize}
\item We can measure the engagement of the users invited through each promotion methods, by for example the number of people that successfully invite at least one friend. 
\item We can measure the quality of the invited users for each promotion method by the number of times they use Uber, how many users they further invite or how much revenue they bring the following $t$ amount of time (\textit{e.g.} 1 month)
\item We can measure the direct effect on the revenue of the company, as the total cost of the promotion per invite
\item We can measure the indirect effect on the revenue of the company as the total revenue brought by users and their invites during the experiment's duration
\item Ultimately we would like to measure the Life Time Value of our users in both promotion variants --- which is complex, involves business direction elements and it's outside the scope of this report. 
\end{itemize} 
\item We can take two random independent samples of users, for example two times 10\% of the client base and inform them that their bonus for every invite is a free ride or a Surge Protector. If the number of users is large enough, we can assume that the samples are identical, and any difference is coming from the different treatments (ceteris paribus).

We can then measure the number of successful invites per user in each group for a predefined period of time, \textit{e.g.} two weeks\footnote{It's a good idea to also keep an outer experiment running, where we check the two groups against the rest of our user-base to check whether our promotion invitations alter their behaviour.}. The number of successful invites can be larger than the number of users (as a single user can invite multiple new users) thus the underlying distribution is Poisson.\footnote{If instead we want to see how many users engaged in invites, we simplify the distribution to a Bernoulli, and a simple g-test as in most A/B testing suites would suffice}

We first need to check if the new promotion idea (Surge Protector $sp$) has a higher expected value ($\lambda_{sp}$) than the existing one (Free Rides, $fr$, $\lambda_{fr}$). We can perform a one-tail test between the two Poisson distributions where:
\begin{itemize}
\item $H_0$: $\lambda_{fr} \geq \lambda_{sp}$
\item $H_1$: $\lambda_{sp} > \lambda_{fr}$
\end{itemize}

The test has the usual steps:
\begin{enumerate}
\item We calculate the two population parameters
\begin{eqnarray}
E[fr] = \lambda_{fr} = \frac{\mbox{Succsful invites}_{fr}}{\mbox{Users}_{fr}}\\
E[sp] = \lambda_{sp} = \frac{\mbox{Succsful invites}_{sp}}{\mbox{Users}_{sp}} \\
\end{eqnarray}
\item We estimate the critical value:
\begin{eqnarray}
Z = \frac{\lambda_{sp} - \lambda_{fr}}{\sqrt{\frac{\lambda_{fr}}{n}}}
\end{eqnarray}
\item We compare against our desired confidence interval to see whether we can reject the null hypothesis (for $a=0.05$, $Z_{90} = 1.65$)
\end{enumerate}
\end{enumerate}
\section{Data analysis}
[120 minutes]

\subsection{Data Cleaning and Visualisation}
Firstly, I took a look at the json file with emacs. It looks like the NaNs are encoded as {\tt NaN} instead of the more common choices of {\tt "NaN"} or {\tt Null}. Thus the first step was to replace the {\tt NaN} with {\tt "NaN"}.

The dataset contains 50k instances:
\begin{lstlisting}[language=bash]
$ tr -s ' ' '\n' <  uber_data_challenge.json| grep city | wc -l
50000
\end{lstlisting}
thus I would go for \textbf{\textsf{R}} for the data analysis and visualisation part, with my code submitted in {\tt data\_cleaning\_and\_visualisation.R}.

\subsubsection{What fraction of the observed users were retained?}
Some observations from the data summary:
\begin{itemize}
\item We have three cities, Astapor, King's Landing and Winterfell. The north seems a bigger fan of Uber {\tt :-p}. 
\item We have two main Phone types, iPhone vs Androids. 
\item The data was pulled 2014-07-01 (as that's the max {\tt last\_trip\_date})
\item The \textbf{retention} is 37.608\%, 18.804 people took a trip in June 2014, out of the 50.000 people who registered with the service in January. 
\item The different cities and the different phone types have different retention levels - it might worth model them separately.
\end{itemize}

\subsection{Building a Predictive Model}
\subsubsection{What Alternatives did you consider?}
We are predicting a labelled binary dataset. It's a very clear \textit{classification} problem. Random Forests have been proven to be the best classification method when it comes of datasets of this size \cite{delgado14a}. Unfortunately, they are hard(er) to interpret, and often their real time evaluation is slow. 

A very widely used method for classification is the SVM with Gaussian Kernels, which performs especially well in continuous, large dimensional spaces with sparse data. In our problem, we have factorial variables which cannot be directly mapped to a Gaussian Kernel. The SVM, although robust to a large number of hyper-parameter settings, produces even harder to interpret models.

In early data exploration, where the objective is the analysis rather than the real-time predictive modelling, the main working horse remains the Logistic Regression. The resulting input weights are easy to interpret, the output has a clear probabilistic interpretation and the learning is fast and reasonably robust. Moreover, it becomes the classifier of choice in massive datasets as there exist excellent online implementations using stochastic gradient descent and it can be expressed as a generalised linear model for real-time distributed deployment.  

Last, but not least we can use Naive Bayes. The model itself is even simpler than the logistic regression as it assumes complete input independence, but it has the advantage of performing classification while dealing gracefully with missing parameters (\textit{e.g.} users who never left feedback for a driver). 

For the objectives of the assignment (analysis, straightforward modelling, no production implementation and prioritisation of insights) my choice is \textbf{logistic regression}.

\subsubsection{Concerns}
A minor concern I have is that we have users that registered on different dates, and might have taken their last trip on a different day, thus we are looking at a different retention windows per user.  In the extreme case, a user that registered on January 31 can have his last trip on June 1st, which is 4 months and two days apart, while a user that registered January 1 might not travel until June 29th (that's larger than the previous window) and be considered retained if he travels on June 30. 

Secondly all our users come from January and are tested for June, which might be strongly affected by seasonality in the three cities of the dataset. 

Lastly, we also have some missing data, but it's only a small part of the final dataset.

\subsection{Logistic Regression}
The Logistic Regression indirectly implies that there is a clear monotonous relationship between the continuous input variables and the target. In cases that this doesn't hold, the results can be greatly improved by bucketing the input variable and representing it as a factorial one. Luckily, it is easy to visually inspect the variables to decide this with the respective plots visible in figure \ref{figplots}.


We then run our logistic regression using all the input variables, all the data, and our dummy variables too. This is a good way to see which parameters seem to be important and how much they affect the probability of retention. A print screen of the \textbf{\textsf{R}} results is visible in \ref{figRLogit}. It seems that the important features are: {\tt city, trips\_in\_first\_30\_days, phone, surge\_pct, uber\_black\_user, weekday\_pct, surge\_pct\_dummy, weekday\_pct\_dummy}. Therefore, and in order to avoid overfitting, these are the inputs we will use for our predictive model.
\begin{figure}[h]
\includegraphics[width=\linewidth]{Routput}
\caption{Evaluation of the different inputs significance for the logistic regression\label{figRLogit}}
\end{figure}


\begin{figure}
\includegraphics[width = .48 \linewidth]{plot_30_days}
\includegraphics[width = .48 \linewidth]{avg_rating_by_driver}
\includegraphics[width = .48 \linewidth]{avg_surge}
\includegraphics[width = .48 \linewidth]{surge_pct}
\includegraphics[width = .48 \linewidth]{weekday_pct}
\includegraphics[width = .48 \linewidth]{avg_rating_by_driver}
\includegraphics[width = .48 \linewidth]{avg_dist_1}
\includegraphics[width = .48 \linewidth]{avg_dist_2}
\caption{\label{figplots} The effect of different inputs to the retention probability. We are looking for monotonous changes, and if this is not the case, we try to account for it by creating buckets of inputs [see code for details]}
\end{figure}
\subsubsection{How Valid is your model?}
In most cases, the easiest way to evaluate our model is through cross validation. We can use for example 25\% of the data for testing and 75\% for training, and perform 4-fold cross validation. We can see then how well we predict user retention. 

Alternatively, we can try to produce one model per {\tt city} (as they are very different) or one model per {\tt phone} type. If the other parameters have the same effect cross-{\tt city} or cross-{\tt phone} I would expect a single model to perform better, as it is trained with more data and the {\tt city} and {\tt phone} coefficients account for the respective retention differences. If the other parameters affect different cities or phone users differently, then we should be able to better model our users with a set of independent models. 

Our cross-valdiation data log-likelihood using the whole dataset is -25145.74, while using a different model per city or per phone doesn't improve the results across the board but only for specific cases [see code]. We thus choose for a single model for the whole dataset, instead of forking our approach. 

\begin{figure}[t]
\begin{center}
\includegraphics[width = .7\linewidth]{ROC}
\end{center}
\caption{The ROC of our predictor\label{figroc}}
\end{figure}

Running cross-validation for multiple folds choices (\textit{e.g.} 4, 10, 20) doesn't change the accuracy significantly, so we can stick with 4-fold cross validation for further evaluation of our classification approach. More specifically, we can run a 4-fold cross validation, get the model predictions and calculate any validation metric we want. 

In this case, I calculated our maximum accuracy, which is 75\% at a cutoff threshold of 0.511.  I further produced a ROC plotted in figure \ref{figroc}, with the area under the curve being 0.81. 

We further might want to see how well we can predict the users we don't retain and try to aim them with a retention specific campaign. If we set our threshold to 0.1, that is, aim the users most likely to leave, we would discover 6908 users, 6370  of which indeed were not retained based on our definition of retainment (92.2\% precision for 20.5\% recall). The threshold of 0.1 can be optimised if we make a precision-to-recall cost-benefit analysis. 
\subsection{How might Uber be able to leverage the insights gained from the model?}
There are multiple insights gained from this analysis that could lead to actionables in different Uber departments. It is important to notice that this type of a-posteriori analysis gives signs of correlation but not necessarily causation. We need to set up the proper experiments to verify whether our insights have the expected effect on customer retention.

\begin{itemize}
\item In terms of marketing, it looks like King's Landing has the highest retention. We should examine the differences between King's landing and the other cities to check for transferable ideas. 
\item The iPhone users are much more likely to be loyal. This might mean that we need to focus on the usability of our Android app. 
\item In terms of surging, the {\tt avg\_surge} doesn't affect retention so much. On the contrary the dummy variable we introduced has a very large effect --- people who have never used Uber during a surge pricing period, are less loyal. This could very well motivate the previous idea that a different invitation promotion like Surge Protection could make the users more loyal. Also, 69.3\% of the user-base has never used an uber during surging prices, thus the idea has a large target group and can be easily prioritised.
\item Interestingly the people who only used Uber in the weekend or only during the weekdays are also highly likely to be less loyal. Perhaps we should give incentives to weekend users to use an Uber during the week, and the vice versa, and thus show them that it's a great transportation alternative for situations they hadn't thought before. This idea would reach only 18.4\% of the user base, and therefore it would be prioritised lower than the previous.
\item Lastly, people who use an Uber black the first month exhibit higher retention. Perhaps a free "uber upgrade" could be a good invitation benefit as well. This idea would affect 62.4\% of our user base that have never used a Uber black, and could potentially have a massive effect. The caveat is of course the higher price of the Uber black, that might indicate a different population of users, and we might be looking at correlation but no causation in their choices. 
\end{itemize}
\bibliography{uber}
\bibliographystyle{plain}
\end{document}
