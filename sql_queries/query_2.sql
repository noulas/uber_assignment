SELECT *
FROM
  (SELECT city_id,
          week,
          driver_id,
          ct number_of_completed_trips,
          rank() over (partition BY city_id, week
                       ORDER BY ct DESC) rank
   FROM
     (SELECT city_id,
             driver_id,
             week,
             count(*) ct
      FROM
        (SELECT city_id,
                driver_id,
                EXTRACT (week
                         FROM request_at) week
         FROM trips
         WHERE city_id IN (1,
                           6,
                           12)
           AND status = 'completed'
           AND request_at > '2013-06-03'
           AND request_at < '2013-06-25') good_data
      GROUP BY city_id,
               driver_id,
               week) counts)rankings
WHERE rank < 4;