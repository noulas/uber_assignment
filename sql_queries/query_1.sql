SELECT city_id,
       cancelled/all_res::float cancellation_percentage
FROM
  ( SELECT result.city_id,
           count(*) all_res,
           sum(result.is_cancelled) cancelled
   FROM
     ( SELECT tr.city_id,
              CASE WHEN (tr.status = 'completed') THEN 0 ELSE 1 END is_cancelled
      FROM
        ( SELECT *
         FROM trips
         WHERE request_at AT time ZONE 'PDT' > '2013-10-01 10:00'
           AND request_at AT time ZONE 'PDT' < '2013-10-22 17:00' ) tr
      JOIN
        ( SELECT *
         FROM users
         WHERE banned = FALSE ) us ON tr.client_id = us.usersid ) RESULT
   GROUP BY RESULT.city_id ) counts
ORDER BY city_id;